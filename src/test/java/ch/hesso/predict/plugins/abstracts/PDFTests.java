/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.abstracts;

import com.google.common.net.MediaType;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HTTP;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.pdf.PDFParser;
import org.apache.tika.sax.BodyContentHandler;
import org.junit.Test;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class PDFTests {

	@Test
	public void testGetPDFParse () throws IOException, TikaException, SAXException {
		HttpClient client = HttpClients.createMinimal ();
		HttpGet resolve = new HttpGet ( "http://internetsociety.org/sites/default/files/06_3_0.pdf" );
		resolve.addHeader ( HTTP.USER_AGENT, "WOOP WOOP WOOP" );
		HttpResponse response = client.execute ( resolve );
		Header contentType = response.getFirstHeader ( "Content-Type" );
		HttpEntity entity = response.getEntity ();
		boolean isPDF = false;
		if ( contentType != null ) {
			isPDF = contentType.getValue ().equals ( MediaType.PDF.toString () );
		}
		if ( isPDF ) {
			InputStream is = entity.getContent ();

			BodyContentHandler handler = new BodyContentHandler ( );
			Metadata metadata = new Metadata ();
			ParseContext context = new ParseContext ();

			//DefaultDetector detector = new DefaultDetector ();
			PDFParser parser = new PDFParser ();
			//Tika tika = new Tika ( detector, parser );
			parser.parse ( is, handler, metadata, context );
			is.close ();

		}
	}
}
