/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.abstracts;

import ch.hesso.predict.client.APIClient;
import ch.hesso.predict.client.resources.DOIClient;
import ch.hesso.predict.client.resources.VenueClient;
import ch.hesso.predict.client.resources.WebPageClient;
import ch.hesso.predict.plugins.abstracts.extractors.*;
import ch.hesso.predict.restful.Doi;
import ch.hesso.predict.restful.Publication;
import ch.hesso.predict.restful.Venue;
import ch.hesso.websocket.entities.AnnotatedPlugin;
import ch.hesso.websocket.plugins.PluginInterface;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class TestVenueExtraction {

	private static final List<Extractor> extractorList = new ArrayList<> ();

	@BeforeClass
	public static void createThings () {
		WebPageClient webPageClient = WebPageClient.create ( APIClient.REST_API );
		extractorList.add ( new BibsonomyExtractor ( webPageClient ) );
		extractorList.add ( new ACMExtractor ( webPageClient ) );
		extractorList.add ( new ElsevierExtractor ( webPageClient ) );
		extractorList.add ( new IEEEXploreExtractor ( webPageClient ) );
		//extractorList.add ( new PDFExtractor ( webPageClient ) );
		extractorList.add ( new SpringerExtractor ( webPageClient ) );
		Collections.sort ( extractorList );

	}

	@Test
	public void testVenueExtraction () {
		String venueKey = "conf/wbir/2006";
		VenueClient client = VenueClient.create ( APIClient.REST_API );
		Venue venue = client.getByKey ( venueKey );
		//String message = String.format ( "", venueKey );
		//builder.text ( message );
		//PluginInterface.publishPluginStatus ( builder.build () );

		boolean success = venue != null;
		if ( success ) {
			List<Publication> publications = client.getPublications ( venue.getId () );
			run ( publications/*, message*/ );
		} else {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to access venue publications, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
		}

	}

	public void run ( final List<Publication> publications/*, final String venueExtract*/ ) {

		List<Publication> toExtract;
		toExtract = filterExtractable ( publications );
		boolean success = toExtract.size () > 0;
		/*if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to find suitable " )
					.build ();
			return;
		}*/

		toExtract = extractPublication ( toExtract );
		// For each extractor
		success = toExtract.size () > 0;
		// Push
	}

	private List<Publication> extractPublication ( final List<Publication> publications ) {
		for ( Publication publication : publications ) {
			boolean success = false;

			for ( Extractor extractor : extractorList ) {

				success = extractor.canExtract ( publication.getUrl () );
				if ( success ) {

					extractor.extract ( publication );
				}
			}
		}
		return publications;
	}

	public List<Publication> filterExtractable ( final List<Publication> publications ) {
		List<Publication> potential = new ArrayList<> ();
		List<Publication> toExtract = new ArrayList<> ();
		DOIClient doiClient = DOIClient.create ( APIClient.REST_API );
		for ( Publication publication : publications ) {
			if ( publication.getUrl () == null || publication.getUrl ().equals ( "" ) ) {
				String url = extractDOI ( doiClient, publication.getDoi () );
				publication.setUrl ( url );
			}
			if ( !"".equals ( publication.getUrl () ) ) {
				potential.add ( publication );
			}
		}

		for ( Publication publication : potential ) {
			for ( Extractor extractor : extractorList ) {
				if ( extractor.canExtract ( publication.getUrl () ) ) {
					toExtract.add ( publication );
				}
			}
		}
		return toExtract;
	}

	public String extractDOI ( final DOIClient doiClient, final String doiUrl ) {
		String lookedUp = "";
		if ( doiUrl != null && !doiUrl.equals ( "" ) ) {
			Doi doi = new Doi ();
			doi.setDoi ( doiUrl );
			doi = doiClient.getDOI ( doi );
			lookedUp = doi.getForward ();
		}
		return lookedUp;
	}
}
