/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.abstracts;

import ch.hesso.predict.client.APIClient;
import ch.hesso.predict.client.resources.WebPageClient;
import ch.hesso.predict.plugins.abstracts.extractors.*;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class TestExtractors {

	static WebPageClient webPageClient = WebPageClient.create ( APIClient.REST_API );

	static List<Extractor> extractorList = new ArrayList<> ();

	@BeforeClass
	public static void createExtractors () {
		extractorList.add ( new ACMExtractor ( webPageClient ) );
		extractorList.add ( new BibsonomyExtractor ( webPageClient ) );
		extractorList.add ( new ElsevierExtractor ( webPageClient ) );
		extractorList.add ( new IEEEXploreExtractor ( webPageClient ) );
		extractorList.add ( new PDFExtractor ( webPageClient ) );
		extractorList.add ( new SpringerExtractor ( webPageClient ) );
	}

	@Test
	public void testSort () {
		Collections.sort ( extractorList );
		Assert.assertTrue ( extractorList.get ( 0 ) instanceof BibsonomyExtractor );
	}

	@Test
	public void testCanExtractACM () {
		String url = "http://dl.acm.org/citation.cfm?id=2207980";
		boolean success = false;
		for ( Extractor extractor : extractorList ) {
			success |= extractor.canExtract ( url );
			if ( success ) {
				break;
			}
		}
		Assert.assertTrue ( success );
	}

	@Test
	public void testCanExtractElsevier () {
		String url = "http://www.sciencedirect.com/science/article/pii/S1877050912003043";
		boolean success = false;
		for ( Extractor extractor : extractorList ) {
			success |= extractor.canExtract ( url );
			if ( success ) {
				break;
			}
		}
		Assert.assertTrue ( success );
	}

	@Test
	public void testCanExtractIEEEXplore () {
		String url = "http://ieeexplore.ieee.org/xpl/articleDetails.jsp?tp=&arnumber=5946649";
		boolean success = false;
		for ( Extractor extractor : extractorList ) {
			success |= extractor.canExtract ( url );
			if ( success ) {
				break;
			}
		}
		Assert.assertTrue ( success );
	}

	@Test
	public void testCanExtractPDF () {
		String url = "http://internetsociety.org/sites/default/files/06_3_0.pdf";
		boolean success = false;
		for ( Extractor extractor : extractorList ) {
			success |= extractor.canExtract ( url );
			if ( success ) {
				break;
			}
		}
		Assert.assertTrue ( success );
	}

	@Test
	public void testCanExtractSpringer () {
		String url = "http://link.springer.com/chapter/10.1007/3-540-60178-3_85";
		boolean success = false;
		for ( Extractor extractor : extractorList ) {
			success |= extractor.canExtract ( url );
			if ( success ) {
				break;
			}
		}
		Assert.assertTrue ( success );
	}

	@Test
	public void testExtractPDF () {
		String url = "http://internetsociety.org/sites/default/files/06_3_0.pdf";
		//String url = "http://dblp.uni-trier.de/xml/docu/dblpxmlreq.pdf";
		//String url = "http://machinelearning.wustl.edu/mlpapers/paper_files/BleiNJ03.pdf";
		PDFExtractor pdfExtractor = new PDFExtractor ( webPageClient );
		//String content = pdfExtractor.extract ( url ); TODO Publication
		//System.out.println ( content );
	}
}
