/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.abstracts;

import com.google.common.io.Files;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HTTP;

import java.io.*;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Random;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class DOIResolve {

	private static List<String> userAgents;

	private static Random r = new Random ();

	public static void main ( String[] args ) throws IOException {
		userAgents = Files.readLines ( new File ( args[ 1 ] ), Charset.defaultCharset () );
		String[] files = { "dois_2011.txt", "dois_2010.txt", "dois_2009.txt", "dois_2008.txt", "dois_2007.txt",
				"dois_2006.txt", "dois_2005.txt", "dois_2004.txt", "dois_2003.txt" };
		for ( String file : files ) {
			FileInputStream fis = new FileInputStream ( new File ( file ) );
			BufferedReader reader = new BufferedReader ( new InputStreamReader ( fis ) );
			FileOutputStream fos = new FileOutputStream ( new File ( file + ".resolved" ) );
			BufferedWriter writer = new BufferedWriter ( new OutputStreamWriter ( fos ) );
			FileOutputStream fosReject = new FileOutputStream ( new File ( file + ".reject" ) );
			BufferedWriter writerReject = new BufferedWriter ( new OutputStreamWriter ( fosReject ) );
			process ( reader, writer, writerReject, Integer.parseInt ( args[ 2 ] ) );
			writer.flush ();
			writerReject.flush ();
			writer.close ();
			writerReject.close ();
			reader.close ();
			fos.close ();
			fosReject.close ();
			fis.close ();
		}
	}

	private static void process (
			final BufferedReader reader,
			final BufferedWriter writer,
			final BufferedWriter reject,
			final int status
	) {
		try {
			String line;
			while ( ( line = reader.readLine () ) != null ) {
				try {
					HttpClient client = HttpClients.createMinimal ();
					HttpGet resolve = new HttpGet ( line );
					resolve.addHeader ( HTTP.USER_AGENT, userAgents.get ( r.nextInt ( userAgents.size () ) ) );
					HttpResponse response = client.execute ( resolve );
					int responseCode = response.getStatusLine ().getStatusCode ();
					if ( responseCode != status ) {
						reject.write ( line );
						reject.newLine ();
					} else {
						Header location = response.getFirstHeader ( "location" );
						if ( location != null && location.getValue () != null ) {
							writer.write ( line + "\t" + location.getValue () );
							writer.newLine ();
						} else {
							reject.write ( line );
							reject.newLine ();
						}
					}
					writer.flush ();
					reject.flush ();
					Thread.sleep ( 1000 );
				} catch ( Exception e ) {
					// HAHA LAWL
				}
			}
		} catch ( Exception ez ) {
			// LAWL
		}
	}

}
