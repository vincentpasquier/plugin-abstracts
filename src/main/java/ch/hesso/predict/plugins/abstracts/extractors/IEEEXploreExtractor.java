/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.abstracts.extractors;

import ch.hesso.predict.client.resources.WebPageClient;
import ch.hesso.predict.plugins.abstracts.events.ExtractorEvent;
import ch.hesso.predict.plugins.abstracts.processor.AbstractProcessor;
import ch.hesso.predict.restful.Publication;
import ch.hesso.predict.restful.WebPage;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class IEEEXploreExtractor extends Extractor {

	private static final String IEEE_URL =
			"ieeexplore.ieee.org";

	public IEEEXploreExtractor ( final WebPageClient webPageClient ) {
		super ( webPageClient );
	}

	@Override
	public boolean canExtract ( final String url ) {
		return url.contains ( IEEE_URL );
	}

	@Override
	public void run () {
		if ( _publications.isEmpty () ) {
			return;
		}
		Publication publication = _publications.pop ();
		WebPage webPage = new WebPage ();
		webPage.setUrl ( publication.getUrl () );
		webPage = _webPageClient.getWebPage ( webPage );

		if ( _webPageClient.isValidWebPage ( webPage ) ) {
			Document doc = Jsoup.parse ( webPage.getContent () );
			Elements tabs = doc.select ( "#nav-article" ).first ().select ( "li" );
			for ( Element tab : tabs.select ( "a" ) ) {
				if ( tab.select ( "#abstract-details-tab" ).size () > 0 ) {
					String abs = doc.select ( ".article" ).first ().text ();
					publication.setContent ( abs );
				}
			}
		}

		AbstractProcessor.AbstractEvents.BUS.publish ( new ExtractorEvent ( publication ) );
	}
}
