/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.abstracts.processor;

import ch.hesso.predict.client.APIClient;
import ch.hesso.predict.client.resources.*;
import ch.hesso.predict.plugins.abstracts.events.ExtractorEvent;
import ch.hesso.predict.plugins.abstracts.extractors.ACMExtractor;
import ch.hesso.predict.plugins.abstracts.extractors.Extractor;
import ch.hesso.predict.plugins.abstracts.extractors.IEEEXploreExtractor;
import ch.hesso.predict.plugins.abstracts.extractors.SpringerExtractor;
import ch.hesso.predict.restful.Doi;
import ch.hesso.predict.restful.Person;
import ch.hesso.predict.restful.Publication;
import ch.hesso.predict.restful.Venue;
import ch.hesso.websocket.entities.AnnotatedPlugin;
import ch.hesso.websocket.plugins.PluginInterface;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class AbstractProcessor {

	private final List<Extractor> extractorList = new ArrayList<> ();

	private final PluginInterface.PluginStatus.Builder builder
			= PluginInterface.newBuilderPluginStatus ( AnnotatedPlugin.State.RUNNING );

	private int countPublications = 0;

	private AtomicInteger countEvents = new AtomicInteger ( 0 );

	private SessionClient sessionClient;

	private PublicationClient publicationClient;

	public AbstractProcessor () {
		WebPageClient webPageClient = WebPageClient.create ( APIClient.REST_API );
		//extractorList.add ( new BibsonomyExtractor ( webPageClient ) );
		extractorList.add ( new ACMExtractor ( webPageClient ) );
		//extractorList.add ( new ElsevierExtractor ( webPageClient ) );
		extractorList.add ( new IEEEXploreExtractor ( webPageClient ) );
		//extractorList.add ( new PDFExtractor ( webPageClient ) );
		extractorList.add ( new SpringerExtractor ( webPageClient ) );
		Collections.sort ( extractorList );
		AbstractEvents.BUS.register ( this );
	}

	@Subscribe
	public void onPublicationExtraction ( final ExtractorEvent event ) {
		countEvents.incrementAndGet ();
		commitPublication ( event.publication () );
		if ( countEvents.get () == countPublications ) {
			sessionClient.commitSession ();
			countEvents.set ( 0 );
			countPublications = 0;
			builder.status ( AnnotatedPlugin.State.DONE );
			PluginInterface.publishPluginStatus ( builder.build () );
		}
	}

	public void run ( final List<Publication> publications ) {
		builder.progress ( 1 ).totalProgress ( 3 ).status ( AnnotatedPlugin.State.RUNNING );

		List<Publication> toExtract;
		toExtract = filterExtractable ( publications );
		boolean success = toExtract.size () > 0;

		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to find suitable extractors for publications." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
			return;
		}

		builder.progress ( 2 );
		sessionClient = SessionClient.create ( APIClient.REST_API );
		sessionClient.createSession ();
		String sessionId = sessionClient.session ();
		publicationClient = PublicationClient.create ( APIClient.REST_API, sessionId );
		toExtract = extractPublication ( toExtract );
		// For each extractor
		success = toExtract.size () > 0;
		if ( !success ) {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to find suitable extractors for publications." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
			return;
		}

		builder.progress ( 3 );
		builder.subProgress ( 1 );
		builder.totalSubProgress ( countPublications );
		builder.text ( "Committing publications abstracts extracted." );
		PluginInterface.publishPluginStatus ( builder.build () );
	}

	/**
	 * @param venueKey
	 */
	public void runVenue ( final String venueKey ) {
		VenueClient client = VenueClient.create ( APIClient.REST_API );
		Venue venue = client.getByKey ( venueKey );

		boolean success = venue != null;
		if ( success ) {
			List<Publication> publications = client.getPublications ( venue.getId () );
			run ( publications );
		} else {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to access venue publications, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
		}
	}

	/**
	 * @param author
	 */
	public void runAuthor ( final String author ) {
		PeopleClient client = PeopleClient.create ( APIClient.REST_API );
		Person person = client.getByName ( author );

		boolean success = person != null;
		if ( success ) {
			List<Publication> publications = client.getPublications ( person.getId () );
			run ( publications );
		} else {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to access venue publications, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
		}
	}

	/**
	 * @param publicationKey
	 */
	public void runPublication ( final String publicationKey ) {
		PublicationClient client = PublicationClient.create ( APIClient.REST_API );
		Publication publication = client.getByKey ( publicationKey );

		boolean success = publication != null;
		if ( success ) {
			run ( Arrays.asList ( publication ) );
		} else {
			PluginInterface.PluginStatus msg = PluginInterface
					.newBuilderPluginStatus ( AnnotatedPlugin.State.ERROR )
					.text ( "Unable to access venue publications, please check logs." )
					.build ();
			PluginInterface.publishPluginStatus ( msg );
		}
	}

	/**
	 * @param publications
	 *
	 * @return
	 */
	public List<Publication> filterExtractable ( final List<Publication> publications ) {
		builder.text ( "Extracting publications, filtering extractable and non-extractable to perform the next step." );
		PluginInterface.publishPluginStatus ( builder.build () );

		List<Publication> potential = new ArrayList<> ();
		List<Publication> toExtract = new ArrayList<> ();
		DOIClient doiClient = DOIClient.create ( APIClient.REST_API );
		for ( Publication publication : publications ) {
			if ( publication.getUrl () == null || publication.getUrl ().equals ( "" ) ) {
				String url = extractDOI ( doiClient, publication.getDoi () );
				publication.setUrl ( url );
			}
			if ( !"".equals ( publication.getUrl () ) ) {
				potential.add ( publication );
			}
		}

		for ( Publication publication : potential ) {
			for ( Extractor extractor : extractorList ) {
				if ( extractor.canExtract ( publication.getUrl () ) ) {
					toExtract.add ( publication );
				}
			}
		}
		return toExtract;
	}

	/**
	 * @param publications
	 *
	 * @return
	 */
	public List<Publication> extractPublication ( final List<Publication> publications ) {
		builder
				.text ( "Extracting publications from publisher websites" )
				.subProgress ( 0 ).totalSubProgress ( publications.size () );
		PluginInterface.publishPluginStatus ( builder.build () );

		countPublications = 0;
		for ( Publication publication : publications ) {
			boolean success = false;

			for ( Extractor extractor : extractorList ) {

				success = extractor.canExtract ( publication.getUrl () );
				if ( success ) {
					extractor.extract ( publication );
					break;
				}
			}
			if ( success ) {
				countPublications++;
				builder.subProgress ( countPublications );
				PluginInterface.publishPluginStatus ( builder.build () );
			}
		}
		return publications;
	}


	/**
	 * @param publication
	 *
	 * @return
	 */

	private void commitPublication ( final Publication publication ) {

		System.out.println ( publication.toString () );

		publicationClient.put ( publication );
		builder.subProgress ( countEvents.get () );
		PluginInterface.publishPluginStatus ( builder.build () );

	}

	/**
	 * @param doiClient
	 * @param doiUrl
	 *
	 * @return
	 */
	public String extractDOI ( final DOIClient doiClient, final String doiUrl ) {
		String lookedUp = "";
		if ( doiUrl != null && !doiUrl.equals ( "" ) ) {
			Doi doi = new Doi ();
			doi.setDoi ( doiUrl );
			doi = doiClient.getDOI ( doi );
			lookedUp = doi.getForward ();
		}
		return lookedUp;
	}

	public enum AbstractEvents {
		BUS;

		private final EventBus _bus;

		private AbstractEvents () {
			_bus = new EventBus ();
		}

		public void register ( final Object object ) {
			_bus.register ( object );
		}

		public void publish ( final Object object ) {
			_bus.post ( object );
		}
	}
}
