/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.abstracts;

import ch.hesso.commons.HTTPMethod;
import ch.hesso.predict.plugins.abstracts.processor.AbstractProcessor;
import ch.hesso.predict.restful.APIEntity;
import ch.hesso.predict.restful.visitors.APIEntityVisitor;
import ch.hesso.websocket.annotations.CallableMethod;
import ch.hesso.websocket.annotations.StringParameter;
import ch.hesso.websocket.plugins.PluginInterface;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class AbstractsPluginInterface extends PluginInterface {

	private final Set<APIEntityVisitor> _visitors = new HashSet<> ();

	private final AbstractProcessor processor = new AbstractProcessor ();

	@StringParameter (value = "conf/conference/venueKey", description = "Specifies the venue key to extract publications from.")
	private String venueKey;

	@StringParameter (value = "name", description = "Specifies the author to extract publications from.")
	private String authorName;

	@StringParameter (value = "conf/venue/something", description = "Publication key to extract abstract from.")
	private String publicationKey;

	@CallableMethod (value = "Extracts abstracts.", description = "Extract abstracts based on parameters given For all publications abstract extraction, refer to administrator.")
	public void extractAbstracts () {

		if ( !"conf/conference/venueKey".equals ( venueKey ) && !"".equals ( venueKey ) ) {
			processor.runVenue ( venueKey );

		} else if ( !"name".equals ( authorName ) && !"".equals ( authorName ) ) {
			processor.runAuthor ( authorName );

		} else if ( !"conf/venue/something".equals ( publicationKey )
				&& !"".equals ( publicationKey ) ) {
			processor.runPublication ( publicationKey );

		}
	}

	@Override
	public String name () {
		return "Abstracts Extractor";
	}

	@Override
	public String description () {
		return "Abstract extraction is done via publication URLs or DOIs. Specialized extractors for the major publishers are designed:<ul><li>IEEEXplore</li><li>Association for Computer Machinery (ACM)</li><li>Springer</li><li>Elsevier</li></ul>PDF file extraction is also available.";
	}

	@Override
	public Map<Class<? extends APIEntity>, Set<HTTPMethod>> resources () {
		return new HashMap<> ();
	}

	@Override
	public Set<APIEntityVisitor> visitors () {
		return _visitors;
	}
}
