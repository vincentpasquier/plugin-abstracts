/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.abstracts.extractors;

import ch.hesso.predict.client.resources.WebPageClient;
import ch.hesso.predict.plugins.abstracts.events.ExtractorEvent;
import ch.hesso.predict.plugins.abstracts.processor.AbstractProcessor;
import ch.hesso.predict.restful.Publication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class BibsonomyExtractor extends Extractor {

	private static final String BIBSONOMY_FILE = "/mnt/Damocles/master/bibsonomy/bibsonomydump.csv";

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( BibsonomyExtractor.class );

	private Map<String, String> urlLine = new HashMap<> ();

	public BibsonomyExtractor ( final WebPageClient webPageClient ) {
		super ( webPageClient );
		try {
			FileInputStream fis = new FileInputStream ( new File ( BIBSONOMY_FILE ) );
			BufferedReader reader = new BufferedReader ( new InputStreamReader ( fis ) );
			String line;
			while ( ( line = reader.readLine () ) != null ) {
				String[] content = line.trim ().split ( "\"" );
				if ( content.length >= 4 ) {
					String url = content[ 1 ];
					urlLine.put ( url, content[ 3 ] );
				}
			}
		} catch ( FileNotFoundException e ) {
			LOG.debug ( "Error while bibsonomy file", e );
		} catch ( IOException e ) {
			LOG.debug ( "Bibsonomy file read error.", e );
		}
	}

	@Override
	public int priority () {
		return Integer.MIN_VALUE;
	}

	@Override
	public boolean canExtract ( final String url ) {
		return false;
	}

	@Override
	public void run () {
		if ( _publications.isEmpty () ) {
			return;
		}
		Publication publication = _publications.pop ();

		if ( urlLine.containsKey ( publication.getUrl () ) ) {
			String abs = urlLine.get ( publication.getUrl () );
			publication.setContent ( abs );
		}

		AbstractProcessor.AbstractEvents.BUS.publish ( new ExtractorEvent ( publication ) );
	}
}
