/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.abstracts.extractors;

import ch.hesso.predict.client.APIClient;
import ch.hesso.predict.client.resources.PDFFileClient;
import ch.hesso.predict.client.resources.WebPageClient;
import ch.hesso.predict.restful.PDFFile;
import ch.hesso.predict.restful.Publication;
import com.google.common.io.BaseEncoding;
import com.google.common.net.HttpHeaders;
import com.google.common.net.MediaType;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.impl.client.HttpClients;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.pdf.PDFParser;
import org.apache.tika.sax.BodyContentHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class PDFExtractor extends Extractor {

	// SLF4J Logger
	private static final Logger LOG = LoggerFactory.getLogger ( PDFExtractor.class );

	private final HttpClient _client = HttpClients.createMinimal ();

	private final PDFFileClient pdfFileClient = PDFFileClient.create ( APIClient.REST_API );

	private final PDFParser _parser;

	public PDFExtractor ( final WebPageClient webPageClient ) {
		super ( webPageClient );
		_parser = new PDFParser ();
	}

	@Override
	public boolean canExtract ( final String url ) {
		boolean success = false;
		HttpHead head = new HttpHead ( url );
		try {
			HttpResponse response = _client.execute ( head );
			Header header = response.getFirstHeader ( HttpHeaders.CONTENT_TYPE );
			success = header != null;
			if ( success ) {
				success = MediaType.PDF.toString ().equals ( header.getValue () );
			}
		} catch ( IOException e ) {
		}
		return success;
	}

	@Override
	public void extract ( final Publication publication ) {
		/*String extracted = "";
		PDFFile pdfFile = new PDFFile ();
		pdfFile.setUrl ( publication.getUrl () );
		pdfFile = pdfFileClient.getPDFFile ( pdfFile );
		success = pdfFile != null && pdfFile.getContent () != null && !pdfFile.getContent ().equals ( "" );
		try {
			if ( success ) {
				byte[] byteContent = BaseEncoding.base64 ().decode ( pdfFile.getContent () );
				ByteArrayInputStream bais = new ByteArrayInputStream ( byteContent );

				PDFContentHandler contentHandler = new PDFContentHandler ();
				BodyContentHandler handler = new BodyContentHandler ( contentHandler );
				Metadata metadata = new Metadata ();
				ParseContext context = new ParseContext ();
				_parser.parse ( bais, handler, metadata, context );
				extracted = contentHandler.builder.toString ();
			}
		} catch ( IOException | TikaException | SAXException e ) {
			LOG.debug ( "Error while reading PDF content, check logs.", e );
		}
		return extracted;*/
	}

	@Override
	public int priority () {
		return 1;
	}

	@Override
	public void run () {

	}

	private static final class PDFContentHandler implements ContentHandler {

		private boolean startP = false;

		private boolean startAbs = false;

		private int next = 0;

		private boolean found = false;

		private StringBuilder builder = new StringBuilder ();

		@Override
		public void setDocumentLocator ( final Locator locator ) {

		}

		@Override
		public void startDocument () throws SAXException {

		}

		@Override
		public void endDocument () throws SAXException {
		}

		@Override
		public void startPrefixMapping ( final String prefix, final String uri ) throws SAXException {

		}

		@Override
		public void endPrefixMapping ( final String prefix ) throws SAXException {

		}

		@Override
		public void startElement ( final String uri, final String localName, final String qName, final Attributes atts ) throws SAXException {
			startP = localName.equals ( "p" );
		}

		@Override
		public void endElement ( final String uri, final String localName, final String qName ) throws SAXException {
			startP = false;
			if ( next > 0 ) {
				next--;
			} else {
				startAbs = false;
			}
		}

		@Override
		public void characters ( final char[] ch, final int start, final int length ) throws SAXException {
			String chars = new String ( ch );
			if ( startAbs ) {
				builder.append ( chars );
			} else if ( !found ) {
				startAbs = chars.toLowerCase ().contains ( "abstract" );
				next = 1;
				found = startAbs;
			}
		}

		@Override
		public void ignorableWhitespace ( final char[] ch, final int start, final int length ) throws SAXException {

		}

		@Override
		public void processingInstruction ( final String target, final String data ) throws SAXException {
		}

		@Override
		public void skippedEntity ( final String name ) throws SAXException {

		}
	}
}
