/**
 * The MIT License (MIT)
 * Copyright (c) 2013-2014 vincent.pasquier@gmail.com
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Project URL: vincent.pasquier.home.hefr.ch
 */
package ch.hesso.predict.plugins.abstracts.extractors;

import ch.hesso.predict.client.resources.WebPageClient;
import ch.hesso.predict.plugins.abstracts.events.ExtractorEvent;
import ch.hesso.predict.plugins.abstracts.processor.AbstractProcessor;
import ch.hesso.predict.restful.Publication;
import ch.hesso.predict.restful.WebPage;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * @author Vincent Pasquier <vincent.pasquier@gmail.com>
 */
public class ElsevierExtractor extends Extractor {

	private static final String[] ELSEVIER_URLS
			= { "linkinghub.elsevier.com", "sciencedirect.com" };

	public ElsevierExtractor ( final WebPageClient webPageClient ) {
		super ( webPageClient );
	}

	@Override
	public boolean canExtract ( final String url ) {
		boolean extract = false;
		for ( String elsevier : ELSEVIER_URLS ) {
			extract |= url.contains ( elsevier );
		}
		return extract;
	}

	public String asciiAbstract ( final String id, final String md5 ) throws IOException {
		Document doc = Jsoup.connect ( "http://www.sciencedirect.com/science" )
				.userAgent ( "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0" )
				.data ( "_ob", "DownloadURL" )
				.data ( "_method", "finish" )
				.data ( "_acct", "C000228598" )
				.data ( "_userid", "10" )
				.data ( "_docType", "FLA" )
				.data ( "_eidkey", id ) // ID
				.data ( "count", "1" )
				.data ( "md5", md5 ) // MD5
				.data ( "JAVASCRIPT_ON", "" )
				.data ( "format", "cite-abs" )
				.data ( "citation-type", "ASCII" )
				.data ( "Export", "Export" )
				.data ( "RETURN_URL", "http://www.sciencedirect.com/science/home" )
				.post ();
		String text = doc.body ().text ();
		int posAbs = text.toLowerCase ().indexOf ( "abstract" );
		int posKeywords = text.toLowerCase ().lastIndexOf ( "keywords" );
		return text.substring ( posAbs, posKeywords > 0 ? posKeywords : text.length () );
	}

	@Override
	public void run () {
		if ( _publications.isEmpty () ) {
			return;
		}
		Publication publication = _publications.pop ();

		WebPage webPage = new WebPage ();
		webPage.setUrl ( publication.getUrl () );
		_webPageClient.getWebPage ( webPage );
		if ( _webPageClient.isValidWebPage ( webPage ) ) {
			String[] tokens = webPage.getUrl ().split ( "/" );
			String id = tokens[ tokens.length - 1 ];
			String md5 = "";
			try {
				String abs = asciiAbstract ( id, md5 );
				publication.setContent ( abs );
			} catch ( IOException e ) {
			}
		}

		AbstractProcessor.AbstractEvents.BUS.publish ( new ExtractorEvent ( publication ) );
	}
}
